package main

import (
	"net/http"
	"io/ioutil"
	"github.com/buger/jsonparser"
	"net/url"
	"strings"
	"fmt"
	"bytes"
	"encoding/json"
	"strconv"
	"time"
	"math/rand"
	"errors"
)

type checkResponse struct {
	Status int64
	Result checkResponseDetail
}

type checkResponseDetail struct {
	Phone string
	otp string
	limit int
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/api/digiroin/btn/cashout", cashout)
	http.ListenAndServe(":6030", mux)
}

func cashout(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	status := http.StatusInternalServerError
	result := `{ "status":` + strconv.Itoa(status) + `,"result":"Something When Wrong" }`
	phone, _ := jsonparser.GetString(body, "phone")
	client := &http.Client{}
	var jsonStr= []byte("")
	form := url.Values{}
	form.Add("name","digi-android");
	form.Add("password","hujandimalamhari");
	request, err := http.NewRequest("GET", "https://couch.digiro.in/_session", strings.NewReader(form.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	respon, err := client.Do(request)
	fmt.Println(respon.Header.Get("Set-Cookie"))
	req, err := http.NewRequest("GET", `https://couch.digiro.in/digiroin/_design/docs/_view/all_cashtag?key="`+phone+`"`,
		bytes.NewBuffer(jsonStr))
	resp, err := client.Do(req)
	response := checkResponse{}
	if err == nil {
		err = json.NewDecoder(resp.Body).Decode(&response)
		if response.Status == 200 {
			err = sms(strconv.FormatUint(random(),10),phone)
			if err != nil {
				status = http.StatusInternalServerError
				result = `{ "status":` + strconv.Itoa(status) + `,"result":"Something When Wrong" }`
			}else{
				status = http.StatusOK
				result = `{ "status":` + strconv.Itoa(status) + `,"result":"OK" }`
			}

		} else {
			status = http.StatusNotFound
			result = `{ "status":` + strconv.Itoa(status) + `,"result":"Phone not found" }`
		}
	} else {
		status = http.StatusInternalServerError
		result = `{ "status":` + strconv.Itoa(status) + `,"result":"Something When Wrong" }`
	}
	w.WriteHeader(status)
	w.Write([]byte(result))
}
func sms(otp string,phone string)(err error){

	url := "http://sms-api.jatismobile.com/index.ashx?channel=2&uploadby=test&batachname=test&division=OPERASI%20DAN%20LAYANAN%20TEKNOLOGI&sender=KANTOR%20POS&message=SMSgatewaytest&msisdn=6285646770852&password=POSINDO879&userid=POSINDO"
	req, _ := http.NewRequest("GET", url, nil)
	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	message := string(body)
	firstSplit := strings.Split(message,"&")
	secSplit := strings.Split(firstSplit[0],"=")
	status := secSplit[1]

	if(status != string(1)){
		err = errors.New("Error Send SMS")
	}

	return err
}

func random() uint64{
	rand.Seed(time.Now().Unix())
	val := uint64(rand.Intn(999999 - 100000) + 100000)
	return val
}
